const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", (req, res) => {
    if (!req.body.name || typeof req.body.name !== "string")
      return res
        .status(200)
        .send({ error: "Request body had missing or malformed field name" });

    if (!req.body.email || typeof req.body.email !== "string")
      return res
        .status(200)
        .send({ error: "Request body had missing or malformed field email" });

    if (!req.body.password || typeof req.body.password !== "string")
      return res.status(200).send({
        error: "Request body had missing or malformed field password",
      });

    if (req.body.password != req.body.passwordConfirmation) {
      return res.status(422).send({
        error: "Password confirmation did not match",
      });
    }

    const uid = uuid();

    let finalJson = {
      user: {
        id: uid,
        name: req.body.name,
        email: req.body.email,
      },
    };

    mongoClient.db().collection().findOne();

    stanConn.publish(
      "users",
      JSON.stringify({
        eventType: "UserCreated",
        entityId: uid,
        entityAggregate: {
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
        },
      })
    );

    res.status(201).json(finalJson);
  });

  api.delete("/users/:uuid", (req, res) => {
    if (!req.get("Authentication")) {
      return res.status(401).send({ error: "Acess Token not found" });
    }

    const userToken = jwt.decode(req.get("Authentication").split(" ")[1]);
    const userId = req.params.uuid;

    if (userToken.id != userId) {
      return res
        .status(403)
        .send({ error: "Access Token did not match User ID" });
    }

    let finalJason = {
      id: userId,
    };

    mongoClient.db().collection().findOne(userId);

    stanConn.publish(
      "users",
      JSON.stringify({
        eventType: "UserDeleted",
        entityId: userId,
        entityAggregate: {},
      })
    );
    return res.status(200).json(finalJason);
  });

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  return api;
};
